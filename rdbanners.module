<?php

/**
 * @file
 * Will create menus and the block needed to view all of the information.
 */

/**
 * Implements hook_help().
 */
function rdbanners_help($path, $arg) {
  switch ($path) {
    case 'admin/help#rdbanners':
      $output = t('Place a different banner or slider on each & every page
                with the help of RDBANNERS');
      return $output;
  }
  // End switch.
}

/**
 * Implements hook_permission().
 */
function rdbanners_permission() {
  return array(
    'administer rdbanners' => array(
      'title' => t('administer rdbanners'),
      'description' => t('Allows users to change what rdbanners are displayed
              and to create their own.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function rdbanners_menu() {
  $items = array();

  $items['admin/structure/rdbanners'] = array(
    'title' => 'Banners',
    'description' => 'Configure the banners or slider.',
    'page callback' => 'rdbanners_admin_page_main',
    'access arguments' => array('administer rdbanners'),
    'file' => 'include/rdbanners.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 1,
  );

  $items['admin/structure/rdbanners/add'] = array(
    'title' => 'Add Banners',
    'description' => 'Add new banners or slider.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rdbanners_add_banner_form'),
    'access arguments' => array('administer rdbanners'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'include/rdbanners.inc',
  );

  $items['admin/structure/rdbanners/rdbanners_images'] = array(
    'title' => 'Add Banner images',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rdbanners_add_banner_images_form'),
    'access arguments' => array('administer rdbanners'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'include/rdbanners.inc',
    'weight' => 2,
  );

  $items['admin/structure/rdbanners/edit_banner/%'] = array(
    'title' => 'Edit Banners',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rdbanners_edit_banner_form'),
    'access arguments' => array('administer rdbanners'),
    'file' => 'include/rdbanners.inc',
  );

  $items['admin/structure/rdbanners/view/%'] = array(
    'title' => 'View Banners Images',
    'page callback' => 'rdbanners_view_banner_images',
    'access arguments' => array('administer rdbanners'),
    'file' => 'include/rdbanners.inc',
  );

  $items['admin/structure/rdbanners/edit_banners_image/%'] = array(
    'title' => 'Edit Banners Image',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rdbanners_admin_edit_banner_image_form'),
    'access arguments' => array('administer rdbanners'),
    'file' => 'include/rdbanners.inc',
  );

  $items['admin/structure/rdbanners/delete_banners_image/%'] = array(
    'title' => 'Delete Banners Image',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rdbanners_admin_delete_banner_image_form'),
    'access arguments' => array('administer rdbanners'),
    'file' => 'include/rdbanners.inc',
  );

  $items['admin/structure/rdbanners/settings'] = array(
    'title' => 'Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('rdbanners_settings_form'),
    'access arguments' => array('administer rdbanners'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'include/rdbanners.inc',
    'weight' => 3,
  );

  return $items;
}

/**
 * Implements hook_block_info().
 */
function rdbanners_block_info() {
  $blocks = array();

  $query = db_select('rdbanners_list', 'rdbl');
  $query->fields('rdbl', array('id', 'rdbanner_title', 'region', 'pages'));
  $results = $query->execute();

  foreach ($results as $result) {
    if (trim($result->pages) == '') {
      $visibility = BLOCK_VISIBILITY_NOTLISTED;
    }
    else {
      $visibility = BLOCK_VISIBILITY_LISTED;
    }

    $pages = str_replace("|", "\r\n", $result->pages);
    $blocks['RDBANNER_' . $result->rdbanner_title] = array(
      'info' => $result->rdbanner_title,
      'region' => $result->region,
      'visibility' => $visibility,
      'cache' => DRUPAL_NO_CACHE,
      'pages' => $pages,
      'status' => TRUE,
    );
  }
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function rdbanners_block_view($delta) {
  $pos = stripos($delta, "RDBANNER_");
  if ($pos === FALSE) {
    return;
  }

  $rdbanner_title = str_replace("RDBANNER_", "", $delta);
  $query = db_select('rdbanners_images', 'rdbi');
  $query->leftjoin('rdbanners_list', 'rdbl', 'rdbi.rdblid = rdbl.id');
  $query->fields('rdbi');
  $query->fields('rdbl');
  $query->condition('rdbl.rdbanner_title', $rdbanner_title, '=');
  $results = $query->execute();

  $variables = array();
  $i = 0;
  $variables['count'] = $i;
  $variables['rdbanner_title'] = $delta;

  while ($row = $results->fetchAssoc()) {
    $variables['url'][] = $row['URL'];

    $image = file_load($row['fid']);
    $src = image_style_url($row['image_style'], $image->uri);
    $title = check_plain($row['caption']);

    $image_opt_array = array(
      'path' => $src,
      'title' => $title,
    );

    $variables['img'][] = theme('image', $image_opt_array);
    $variables['count'] = ++$i;
  }

  if ($variables['count'] == 0) {
    return;
  }

  $lib_path = libraries_get_path('jquery.bxslider');
  $module_path = drupal_get_path('module', 'rdbanners');

  $rdbanners_options = array(
    'adaptiveHeight' => variable_get('rdbanners_adh', 3000),
    'pause' => variable_get('rdbanners_pause', 3000),
    'caption' => variable_get('rdbanners_caption', 1),
    'pager' => variable_get('rdbanners_pager', 1),
    'autoControl' => variable_get('rdbanners_ac', 0),
    'autoHover' => variable_get('rdbanners_ah', 1),
    'loop' => variable_get('rdbanners_loop', 1),
    'slideMin' => variable_get('rdbanners_cmin', 2),
    'slideMax' => variable_get('rdbanners_cmax', 3),
    'slideWidth' => variable_get('rdbanners_csw', 250),
    'slideMargin' => variable_get('rdbanners_csm', 10),
  );
  $title = str_replace(' ', '_', $rdbanner_title);
  $type = rdbanners_get_rdbanner_type($rdbanner_title);
  $rdbanners_options['sliderType'][] = array('RDBANNER_' . $title => $type);

  if ($type == 'slider') {
    $rdbanners_options['mode'] = variable_get('rdbanners_mode', 'fade');
  }

  if ($variables['count'] > 1) {
    $block['content'] = array(
      '#markup' => theme('rdbanner_slider_output', $variables),
      '#attached' => array(
        'css' => array(
          $module_path . '/css/rdbanner.css',
          $lib_path . '/jquery.bxslider.css',
        ),
        'js' => array(
          $lib_path . '/jquery.bxslider.js',
          $module_path . '/js/rdbanner.js',
          array(
            'data' => array('rdbanners' => $rdbanners_options),
            'type' => 'setting',
          ),
        ),
      ),
    );
    return $block;
  }
  else {
    $block['content'] = array(
      '#markup' => theme('rdbanner_banner_output', $variables),
      '#attached' => array(
        'css' => array($module_path . '/css/rdbanner.css'),
      ),
    );
    return $block;
  }
}

/**
 * Implements hook_theme().
 */
function rdbanners_theme() {
  $theme = array(
    'rdbanner_banner_output' => array(
      'variables' => array(),
      'path' => drupal_get_path('module', 'rdbanners') . '/templates',
      'template' => 'rdbanners_banner',
    ),
    'rdbanner_slider_output' => array(
      'variables' => array(),
      'path' => drupal_get_path('module', 'rdbanners') . '/templates',
      'template' => 'rdbanners_bxslider',
    ),
  );
  return $theme;
}

/**
 * Implements a function to get type of slider i.e. carousel or slider.
 */
function rdbanners_get_rdbanner_type($rdbanner_title) {
  $query = db_select('rdbanners_list', 'rdbl');
  $query->fields('rdbl', array('rdbanner_type'));
  $query->condition('rdbl.rdbanner_title', $rdbanner_title, '=');
  $results = $query->execute()->fetchAssoc();
  return $results['rdbanner_type'];
}
