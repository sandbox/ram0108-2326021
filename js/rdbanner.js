/**
 * @file
 * Some basic behaviors and utility functions for bxslider.
 */

(function($) {
    Drupal.behaviors.rdbanners = {
        attach: function(context, settings) {
            var id = $(this).attr("id");
            for (var i = 0; i < Drupal.settings.rdbanners.sliderType.length; i++) {
                $.each(Drupal.settings.rdbanners.sliderType[i], function(key, value) {
                    if (value == 'slider') {
                        $('#bx-slider-' + key).bxSlider({
                            adaptiveHeight: parseInt(Drupal.settings.rdbanners.adaptiveHeight),
                            auto: true,
                            mode: Drupal.settings.rdbanners.mode,
                            autoControls: parseInt(Drupal.settings.rdbanners.autoControl),
                            pause: parseInt(Drupal.settings.rdbanners.pause),
                            captions: parseInt(Drupal.settings.rdbanners.caption),
                            pager: parseInt(Drupal.settings.rdbanners.pager),
                            autoHover: parseInt(Drupal.settings.rdbanners.autoHover),
                            infiniteLoop: parseInt(Drupal.settings.rdbanners.loop),
                        });
                    }
                    if (value == 'carousel') {
                        $('#bx-slider-' + key).bxSlider({
                            minSlides: parseInt(Drupal.settings.rdbanners.slideMin),
                            maxSlides: parseInt(Drupal.settings.rdbanners.slideMax),
                            slideWidth: parseInt(Drupal.settings.rdbanners.slideWidth),
                            slideMargin: parseInt(Drupal.settings.rdbanners.slideMargin),
                            adaptiveHeight: parseInt(Drupal.settings.rdbanners.adaptiveHeight),
                            auto: true,
                            autoControls: parseInt(Drupal.settings.rdbanners.autoControl),
                            pause: parseInt(Drupal.settings.rdbanners.pause),
                            captions: parseInt(Drupal.settings.rdbanners.caption),
                            pager: parseInt(Drupal.settings.rdbanners.pager),
                            autoHover: parseInt(Drupal.settings.rdbanners.autoHover),
                            infiniteLoop: parseInt(Drupal.settings.rdbanners.loop),
                        });
                    }
                });
            }
        }
    };
})(jQuery);
