<?php

/**
 * @file
 * This template handles the layout for slider with multiple images.
 */
?>
<?php
$rdbanner_title = str_replace(' ', '_', $rdbanner_title);
?>

<ul id="bx-slider-<?php echo $rdbanner_title; ?>" class="bx-slider">
  <?php
  $i = 0;
  while ($i < $count) :
    ?>    
    <li>
      <?php if(isset($url[$i]) && trim($url[$i]) != '') : ?>
      <a href="<?php echo url($url[$i]); ?>" target="_blank">
         <?php echo $img[$i]; ?>
      </a>
      <?php else : ?>
      <?php echo $img[$i]; ?>
      <?php endif; ?>
    </li>
    <?php $i++; ?>
  <?php endwhile ?>        
</ul>
