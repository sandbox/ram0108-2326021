********************************************************************
                         ==RD-BANNER MODULE==
********************************************************************
Original Author: Ram Dubey
Current Maintainers: Ram Dubey

********************************************************************
DESCRIPTION:

   RDBANNER Module provides the facility to the admin to create multiple slider
   for a page. With RDBANNER admin can assign multiple slider on the page 
   according to regions. Admin can choose  & put different slider on different 
   pages.


********************************************************************
INSTALLATION & CONFIGURATION:

1. Place the entire rdbanners directory into your Drupal modules/
   directory or the sites modules directory (eg site/default/modules)

2. download "libraries" & "jquery update" module and place it into Drupal 
   modules directory.

3. download  bxslider library from url : http://bxslider.com and place it into 
   libraries directory i.e. (site/default/libraries). Name of library must be 
   "jquery.bxslider". If not, please rename it.

4. Enable this module by navigating to:

     Administration > Modules

5. Go to admin/config/development/jquery_update and select Default jQuery 
   Version atleast 1.8.  

6. Go to admin/people/permissions#module-rdbanners and assign
   permissions to the roles.


7. Navigate Administration > structure > banner & Please read the step by step
   instructions as an example to use this module below:-

a) Create a new banner with link "Add banners". On add banner form, you need to 
   select region as well as pages where you need to display banner.

b) After that upload banner images with link "Add banner images",From where you 
   need to select banner from drop-down and can upload multiple image at once 
   with caption and image URL.

c) Once you uploaded image, you can check those pages where you had assigned 
   the banners on selected region. If you are not able to see banner then once 
   clear all cache then refresh & check the page.

d) Please visite on RDBANNER setting page and manage bx-slider 
   settings according to you.

8. This module is very useful if you need to put lots of different sliders on 
   different pages. You dont need to create any view or block. This module 
   create blocks according to slider and put in appropriate region. 
   RDBANNER create responsive slider with the help of bx-slider library.
