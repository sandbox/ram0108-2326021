<?php

/**
 * @file
 * Includes all functions for the RD-banner module.
 */

/**
 * Functions to display rdbanners listing page in admin.
 */
function rdbanners_admin_page_main() {
  $output = '';

  $header = array(
    array(
      'data' => t('S.no.'),
    ),
    array(
      'data' => t('Title'),
      'field' => 'rdbanner_title',
      'sort' => 'asc',
    ),
    array(
      'data' => t('Type'),
      'field' => 'rdbanner_type',
    ),
    array(
      'data' => t('Banner Style'),
      'field' => 'image_style',
    ),
    array(
      'data' => t('Region'),
      'field' => 'region',
    ),
    array(
      'data' => t('Pages'),
      'field' => 'pages',
    ),
    array(
      'data' => t('Operation'),
      'colspan' => '2',
    ),
  );

  $query = db_select('rdbanners_list', 'rdbl');
  $query->fields('rdbl')
      ->extend('PagerDefault')->extend('TableSort')
      ->limit(10)
      ->orderByHeader($header);

  $results = $query->execute();
  $row = array();
  $i = 1;
  foreach ($results as $result) {
    $editlink = "admin/structure/rdbanners/edit_banner/" . $result->id;
    $viewlink = "admin/structure/rdbanners/view/" . $result->id;
    $pages = str_replace("|", ", ", $result->pages);
    $pages = str_replace("<front>", "Home", $pages);
    $row[] = array(
      'data' => array(
        $i,
        $result->rdbanner_title,
        $result->rdbanner_type,
        $result->image_style,
        $result->region,
        $pages,
        l(t('edit'), $editlink),
        l(t('view'), $viewlink),
      ),
    );
    $i++;
  }

  $output['rdbanners_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $row,
    '#empty' => t('No Banners Found.'),
  );

  // Adds the pager buttons to the bottom of the table.
  $output['rdbanners_pager'] = array('#theme' => 'pager');

  // Let drupal handle print and echo.
  return $output;
}

/**
 * Function to create rdbanners form.
 */
function rdbanners_add_banner_form($form, &$form_state) {
  $is = image_styles();
  foreach ($is as $key => $data) {
    $image_style[$key] = $data['label'];
  }
  $form = array();

  $form['#tree'] = TRUE;

  if (empty($form_state['page_num'])) {
    $form_state['page_num'] = 1;
  }

  $form['rdbanner_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Banner title'),
    '#description' => t('Put a title for sliders. Must be limit to 20 char.'),
    '#required' => TRUE,
    '#attributes' => array('size' => 20),
  );

  $form['image_style'] = array(
    '#type' => 'select',
    '#title' => t('Image style'),
    '#description' => t('It will applied on all images within this banner.'),
    '#options' => $image_style,
  );

  $slider_type = array('slider' => 'slider', 'carousel' => 'carousel');
  $form['rdbanner_type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#description' => t('Please select option i.e. slider or carousel'),
    '#options' => $slider_type,
  );

  $current_theme = variable_get('theme_default', 'none');
  $regions = system_region_list($current_theme);

  $form['region'] = array(
    '#type' => 'select',
    '#title' => t('Banner region'),
    '#description' => t('region where slider needs to display.'),
    '#options' => $regions,
  );

  $form['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t('Pages Where banner will be display. Enter one path per line.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('submit'),
  );

  return $form;
}

/**
 * Function to validate add banner form.
 */
function rdbanners_add_banner_form_validate($form, &$form_state) {
  if (strlen($form_state['values']['rdbanner_title']) > 20) {
    form_set_error('rdbanner_title', filter_xss(t('Title must be limit to 20 chars.')));
  }
}

/**
 * Function to submit rdbanners.
 */
function rdbanners_add_banner_form_submit($form, &$form_state) {
  $rdbanner_title = $form_state['values']['rdbanner_title'];
  $image_style = $form_state['values']['image_style'];
  $rdbanner_type = $form_state['values']['rdbanner_type'];
  $region = $form_state['values']['region'];
  $pages = $form_state['values']['pages'];

  db_insert('rdbanners_list')->fields(array(
    'rdbanner_title' => $rdbanner_title,
    'image_style' => $image_style,
    'rdbanner_type' => $rdbanner_type,
    'region' => $region,
    'pages' => $pages,
  ))->execute();

  drupal_set_message(filter_xss(t('Banner has been inserted successfully.')), 'status');
  $form_state['redirect'] = 'admin/structure/rdbanners';
}

/**
 * Function to display form for upload image in rdbanners.
 */
function rdbanners_add_banner_images_form($form, &$form_state) {
  $form = array();
  $form['#attributes'] = array('enctype' => "multipart/form-data");
  $query = db_select('rdbanners_list', 'rdbl');
  $query->fields('rdbl', array('id', 'rdbanner_title'));
  $results = $query->execute();

  $options = array();
  while ($result = $results->fetchAssoc()) {
    $options[$result['id']] = $result['rdbanner_title'];
  }
  $image_path = 'public://rdbanners/';
  $ext = array("png gif jpg jpeg");
  $form['#tree'] = TRUE;

  $form['banner'] = array(
    '#type' => 'select',
    '#title' => t('Select banner'),
    '#description' => t('Select banner from list in which you want to upload
                image'),
    '#options' => $options,
    '#weight' => 0,
    '#required' => TRUE,
  );

  $form['fieldset_area'] = array(
    '#type' => 'fieldset',
    '#title' => 'Upload Banner Images',
    '#prefix' => '<div id="banner-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  if (empty($form_state['banner_image_num'])) {
    $form_state['banner_image_num'] = 1;
  }

  for ($i = 0; $i < $form_state['banner_image_num']; $i++) {
    $form['fieldset_area']['banner_image'][$i] = array(
      '#type' => 'fieldset',
      '#title' => t('Banner image & Link'),
      '#weight' => ($i + 2),
    );

    $form['fieldset_area']['banner_image'][$i]['image'] = array(
      '#type' => 'managed_file',
      '#title' => t('Banner image:'),
      '#description' => t('The uploaded image will be displayed with banner.'),
      '#required' => TRUE,
      '#upload_location' => variable_get('rdbanners_image_path', $image_path),
      "#upload_validators" => array("file_validate_extensions" => $ext),
    );

    $form['fieldset_area']['banner_image'][$i]['caption'] = array(
      '#type' => 'textarea',
      '#title' => t('Caption:'),
    );

    $form['fieldset_area']['banner_image'][$i]['link'] = array(
      '#type' => 'textfield',
      '#title' => t('Linked URL:'),
      '#description' => t('URL linked with banner image on slider.'),
    );

    if ($form_state['banner_image_num'] > 1) {
      $form['fieldset_area']['remove_one_banner_fieldset'] = array(
        '#type' => 'submit',
        '#value' => t('Remove Last one'),
        '#submit' => array('rdbanners_remove_last_one_banner_fieldset'),
        '#ajax' => array(
          'callback' => 'rdbanners_add_more_banner_image_callback',
          'wrapper' => 'banner-fieldset-wrapper',
        ),
      );
    }
  }

  $form['fieldset_area']['add_more_banner_fieldset'] = array(
    '#type' => 'submit',
    '#value' => t('Add one more'),
    '#submit' => array('rdbanners_add_more_banner_fieldset'),
    '#ajax' => array(
      'callback' => 'rdbanners_add_more_banner_image_callback',
      'wrapper' => 'banner-fieldset-wrapper',
    ),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Function to add more images callback.
 */
function rdbanners_add_more_banner_image_callback($form, &$form_state) {
  return $form['fieldset_area'];
}

/**
 * Function to add more banner fieldset callback.
 */
function rdbanners_add_more_banner_fieldset($form, &$form_state) {
  $form_state['banner_image_num']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Function to remove banner fieldset callback.
 */
function rdbanners_remove_last_one_banner_fieldset($form, &$form_state) {
  if ($form_state['banner_image_num'] > 1) {
    $form_state['banner_image_num']--;
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Function to add banner images in rdbanners.
 */
function rdbanners_add_banner_images_form_submit($form, &$form_state) {

  foreach ($form_state['values']['fieldset_area']['banner_image'] as $files) {

    $file = file_load($files['image']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);

    db_insert('rdbanners_images')->fields(array(
      'fid' => $file->fid,
      'rdblid' => $form_state['values']['banner'],
      'filename' => $file->filename,
      'caption' => $files['caption'],
      'URL' => $files['link'],
    ))->execute();

    drupal_set_message(filter_xss(t('Image @image_name has been uploaded successfully.', array('@image_name' => $file->filename))));
    $form_state['redirect'] = 'admin/structure/rdbanners';
  }
}

/**
 * Rdbanners settings form.
 */
function rdbanners_settings_form($form, &$form_state) {
  $tf_options = array(
    0 => 'No',
    1 => 'Yes',
  );

  $form['rdbanners_path'] = array(
    '#title' => t('Path to save all banner images'),
    '#description' => t('Path to save all banner or slider images'),
    '#type' => 'textfield',
    '#default_value' => variable_get('rdbanners_image_path', 'public://rdbanners/'),
    '#required' => TRUE,
  );

  $form['bxslider'] = array(
    '#type' => 'fieldset',
    '#title' => 'Bx-Slider Settings',
    '#collapsible' => TRUE,
  );

  $form['bxslider']['adaptive_height'] = array(
    '#type' => 'select',
    '#title' => t('Allow Adaptive Height'),
    '#description' => t("Adjust slider height based on each slide's height."),
    '#options' => $tf_options,
    '#default_value' => variable_get('rdbanners_adh', 1),
  );

  $form['bxslider']['pause'] = array(
    '#type' => 'textfield',
    '#title' => t('Pause'),
    '#description' => t('Duration for next slide in ms.'),
    '#required' => TRUE,
    '#default_value' => variable_get('rdbanners_pause', '3000'),
  );

  $form['bxslider']['caption'] = array(
    '#title' => t('Allow Caption'),
    '#description' => t('To display some text on slider.'),
    '#type' => 'select',
    '#options' => $tf_options,
    '#default_value' => variable_get('rdbanners_caption', 1),
  );

  $form['bxslider']['pager'] = array(
    '#title' => t('Allow Pager'),
    '#description' => t('To display pager at bottom of slider.'),
    '#type' => 'select',
    '#options' => $tf_options,
    '#default_value' => variable_get('rdbanners_pager', 1),
  );

  $mode_options = array(
    'fade' => 'fade',
    'horizontal' => 'horizontal',
    'vertical' => 'vertical',
  );
  $form['bxslider']['mode'] = array(
    '#title' => t('mode'),
    '#description' => t('Type of transition between slides.'),
    '#type' => 'select',
    '#options' => $mode_options,
    '#default_value' => variable_get('rdbanners_mode', 'fade'),
  );

  $form['bxslider']['auto_controls'] = array(
    '#title' => t('Allow Auto controls'),
    '#description' => t('To display Start/Stop control on slider.'),
    '#type' => 'select',
    '#options' => $tf_options,
    '#default_value' => variable_get('rdbanners_ac', 0),
  );

  $form['bxslider']['auto_hover'] = array(
    '#title' => t('Allow Auto hover'),
    '#description' => t('To pause the slider on mouse hover.'),
    '#type' => 'select',
    '#options' => $tf_options,
    '#default_value' => variable_get('rdbanners_ah', 1),
  );

  $form['bxslider']['loop'] = array(
    '#title' => t('Infinite loop'),
    '#description' => t('Transition on first slide after reach on last slide.'),
    '#type' => 'select',
    '#options' => $tf_options,
    '#default_value' => variable_get('rdbanners_loop', 1),
  );

  $form['bxslider_carousel'] = array(
    '#type' => 'fieldset',
    '#title' => 'Bx-Slider Carousel Settings',
    '#collapsible' => TRUE,
  );

  $form['bxslider_carousel']['min_slides'] = array(
    '#type' => 'textfield',
    '#title' => t('Min Slides'),
    '#description' => t('The minimum number of slides to be shown.'),
    '#required' => TRUE,
    '#default_value' => variable_get('rdbanners_cmin', 2),
  );

  $form['bxslider_carousel']['max_slides'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Slides'),
    '#description' => t('The maximum number of slides to be shown.'),
    '#required' => TRUE,
    '#default_value' => variable_get('rdbanners_cmax', 3),
  );

  $form['bxslider_carousel']['slide_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Width'),
    '#description' => t('Width for slide. This should be numeric, not zero.'),
    '#required' => TRUE,
    '#default_value' => variable_get('rdbanners_csw', 250),
  );

  $form['bxslider_carousel']['slide_margin'] = array(
    '#type' => 'textfield',
    '#title' => t('Slide Margin'),
    '#description' => t('Margin between each two slides,  not zero.'),
    '#required' => TRUE,
    '#default_value' => variable_get('rdbanners_csm', 10),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Function to validate rdbanners settings.
 */
function rdbanners_settings_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['pause']) ||
      $form_state['values']['pause'] < 1000) {
    form_set_error('pause', filter_xss(t('Time duration between two slides')));
  }

  if (!is_numeric($form_state['values']['min_slides']) ||
      $form_state['values']['min_slides'] < 1) {
    form_set_error('min_slides', filter_xss(t('Min slides must be numeric i.e. >= 1.')));
  }

  if (!is_numeric($form_state['values']['max_slides']) ||
      $form_state['values']['max_slides'] < 1) {
    form_set_error('max_slides', filter_xss(t('Max slide must be numeric i.e. >= 1.')));
  }

  if (!is_numeric($form_state['values']['slide_width'])) {
    form_set_error('max_slides', filter_xss(t('width for slide must be numeric.')));
  }

  if (!is_numeric($form_state['values']['slide_margin'])) {
    form_set_error('slide_margin', filter_xss(t('margin for slide must be numeric.')));
  }
}

/**
 * Function to save rdbanners settings.
 */
function rdbanners_settings_form_submit($form, &$form_state) {
  variable_set('rdbanners_image_path', $form_state['values']['rdbanners_path']);

  variable_set('rdbanners_adh', $form_state['values']['adaptive_height']);
  variable_set('rdbanners_pause', $form_state['values']['pause']);
  variable_set('rdbanners_caption', $form_state['values']['caption']);
  variable_set('rdbanners_pager', $form_state['values']['pager']);
  variable_set('rdbanners_mode', $form_state['values']['mode']);
  variable_set('rdbanners_ac', $form_state['values']['auto_controls']);
  variable_set('rdbanners_ah', $form_state['values']['auto_hover']);
  variable_set('rdbanners_loop', $form_state['values']['loop']);
  variable_set('rdbanners_cmin', $form_state['values']['min_slides']);
  variable_set('rdbanners_cmax', $form_state['values']['max_slides']);
  variable_set('rdbanners_csw', $form_state['values']['slide_width']);
  variable_set('rdbanners_csm', $form_state['values']['slide_margin']);

  drupal_set_message(filter_xss(t('Rdbanner settings has been saved successfully.')), 'status');
}

/**
 * Function to edit rdbanners.
 */
function rdbanners_edit_banner_form($form, &$form_state) {
  $is = image_styles();
  foreach ($is as $key => $data) {
    $image_style[$key] = $data['label'];
  }
  $form = array();

  $query = db_select('rdbanners_list', 'rdbl');
  $query->fields('rdbl')
      ->condition('rdbl.id', arg(4), '=');
  $result = $query->execute();
  $row = $result->fetchassoc();

  $form['#tree'] = TRUE;

  if (!isset($form_state['page_num'])) {
    $form_state['page_num'] = 1;
  }

  $form['rdblid'] = array(
    '#type' => 'hidden',
    '#default_value' => $row['id'],
  );
  $form['rdbanner_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Banner title'),
    '#description' => t('Put a title for slider'),
    '#required' => TRUE,
    '#default_value' => $row['rdbanner_title'],
    '#attributes' => array('readonly' => 'readonly'),
  );

  $form['image_style'] = array(
    '#type' => 'select',
    '#title' => t('Image style'),
    '#description' => t('image style which will applied on rdbanners images.'),
    '#options' => $image_style,
    '#default_value' => $row['image_style'],
  );

  $slider_type = array('slider' => 'slider', 'carousel' => 'carousel');
  $form['rdbanner_type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#description' => t('Please select option i.e. slider or carousel.'),
    '#options' => $slider_type,
    '#default_value' => $row['rdbanner_type'],
  );

  $current_theme = variable_get('theme_default', 'none');
  $regions = system_region_list($current_theme);

  $form['region'] = array(
    '#type' => 'select',
    '#title' => t('Banner region'),
    '#description' => t('region where slider needs to display. Not Editable.'),
    '#options' => $regions,
    '#default_value' => $row['region'],
  );

  $form['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t('Pages Where banner will be display.'),
    '#default_value' => $row['pages'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('submit'),
  );
  return $form;
}

/**
 * Submit function to edit rdbanners.
 */
function rdbanners_edit_banner_form_submit($form, &$form_state) {
  $delta = 'RDBANNER_' . $form_state['values']['rdbanner_title'];
  $block = block_load('rdbanners', $delta);
  $current_theme = variable_get('theme_default', 'none');

  $rdbanner_block = array(
    'bid' => $block->bid,
    'module' => 'rdbanners',
    'delta' => $delta,
    'theme' => $current_theme,
    'visibility' => 1,
    'region' => $form_state['values']['region'],
    'status' => 1,
    'pages' => $form_state['values']['pages'],
  );
  drupal_write_record('block', $rdbanner_block, 'bid');

  $query = db_update('rdbanners_list');
  $query->fields(array(
    'rdbanner_title' => $form_state['values']['rdbanner_title'],
    'image_style' => $form_state['values']['image_style'],
    'rdbanner_type' => $form_state['values']['rdbanner_type'],
    'region' => $form_state['values']['region'],
  ))->condition('id', $form_state['values']['rdblid'], '=')->execute();

  drupal_set_message(filter_xss(t('banner has been updated successfully...')), 'status');
  $form_state['redirect'] = 'admin/structure/rdbanners';
}

/**
 * Function to edit existing banner image.
 */
function rdbanners_admin_edit_banner_image_form($form, &$form_state) {
  $form = array();
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  $image_path = 'public://rdbanners/';
  $ext = array("png gif jpg jpeg");
  $form['#tree'] = TRUE;

  if (empty($form_state['remove_image'])) {
    $form_state['remove_image'] = 0;
  }

  $form['rdbi_id'] = array(
    '#type' => 'hidden',
    '#default_value' => arg(4),
  );

  $query = db_select('rdbanners_images', 'rdbi');
  $query->fields('rdbi')
      ->condition('rdbi_id', arg(4), '=');
  $result = $query->execute();
  $row = $result->fetchassoc();

  if (isset($row['fid'])) {
    $image = file_load($row['fid']);
    $file = '<img src="' . image_style_url('thumbnail', $image->uri) . '">';

    $form['fid'] = array(
      '#type' => 'hidden',
      '#default_value' => $row['fid'],
    );
  }

  $form['image_fieldset'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="banner-img-wrapper">',
    '#suffix' => '</div>',
  );

  if ($form_state['remove_image'] == 0) {
    $form['image_fieldset']['file'] = array(
      '#type' => 'item',
      '#markup' => $file,
    );

    $form['image_fieldset']['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Remove'),
      '#submit' => array('rdbanners_remove_banner_image'),
      '#ajax' => array(
        'callback' => 'rdbanners_remove_banner_image_callback',
        'wrapper' => 'banner-img-wrapper',
      ),
    );
  }

  if (isset($form_state['remove_image']) && $form_state['remove_image'] == 1) {
    $form['image_fieldset']['image'] = array(
      '#type' => 'managed_file',
      '#title' => t('Banner image:'),
      '#description' => t('The uploaded image will be displayed with banner.'),
      '#required' => TRUE,
      '#upload_location' => variable_get('rdbanners_image_path', $image_path),
      "#upload_validators" => array("file_validate_extensions" => $ext),
    );
  }
  $form['caption'] = array(
    '#type' => 'textarea',
    '#title' => t('Caption:'),
    '#default_value' => $row['caption'],
  );

  $form['link'] = array(
    '#type' => 'textfield',
    '#title' => t('Linked URL:'),
    '#description' => t('URL linked with banner image on slider.'),
    '#default_value' => $row['URL'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Function callback to remove existing banner image.
 */
function rdbanners_remove_banner_image_callback($form, &$form_state) {
  return $form['image_fieldset'];
}

/**
 * Function ajax submit to remove existing banner image.
 */
function rdbanners_remove_banner_image($form, &$form_state) {
  if ($form_state['remove_image'] == 0) {
    $form_state['remove_image']++;

    $query = db_update('rdbanners_images');
    $query->fields(array(
          'fid' => 0,
          'filename' => '',
        ))->condition('rdbi_id', $form_state['values']['rdbi_id'], '=')
        ->condition('fid', $form_state['values']['fid'], '=')->execute();

    $file = file_load($form_state['values']['fid']);
    file_delete($file);
    drupal_set_message(filter_xss(t('Banner images have been deleted successfully.')));
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit function to edit banner image.
 */
function rdbanners_admin_edit_banner_image_form_submit($form, &$form_state) {
  if (isset($form_state['values']['image_fieldset']['image'])) {
    $file = file_load($form_state['values']['image_fieldset']['image']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    $fields_array = array(
      'fid' => $file->fid,
      'filename' => $file->filename,
      'caption' => $form_state['values']['caption'],
      'URL' => $form_state['values']['link'],
    );
  }
  else {
    $fields_array = array(
      'caption' => $form_state['values']['caption'],
      'URL' => $form_state['values']['link'],
    );
  }
  $query = db_update('rdbanners_images');
  $query->fields($fields_array)
      ->condition('rdbi_id', $form_state['values']['rdbi_id'], '=')->execute();

  drupal_set_message(filter_xss(t('Banner images have been updated successfully.')));
  $form_state['redirect'] = 'admin/structure/rdbanners';
}

/**
 * Function to display all images within rdbanners.
 */
function rdbanners_view_banner_images() {
  $header = array(
    array(
      'data' => t('Image'),
      'field' => 'filename',
    ),
    array(
      'data' => t('Caption'),
      'field' => 'caption',
    ),
    array(
      'data' => t('URL'),
      'field' => 'URL',
    ),
    array(
      'data' => t('Operation'),
      'colspan' => '2',
    ),
  );

  $qry = db_select('rdbanners_images', 'rdbi');
  $qry->fields('rdbi')
      ->extend('PagerDefault')->extend('TableSort')
      ->limit(10)
      ->condition('rdblid', arg(4), '=');
  $rsts = $qry->execute();
  $row = array();

  foreach ($rsts as $rst) {
    $image = file_load($rst->fid);
    $file = '<img src="' . image_style_url('thumbnail', $image->uri) . '">';
    $edit_path = "admin/structure/rdbanners/edit_banners_image/";
    $delete_path = "admin/structure/rdbanners/delete_banners_image/";
    $caption = check_plain($rst->caption);
    $url = check_url($rst->URL);
    $row[] = array(
      'data' => array(
        $file,
        $caption,
        $url,
        l(t('Edit'), $edit_path . $rst->rdbi_id),
        l(t('Delete'), $delete_path . $rst->rdbi_id),
      ),
    );
  }

  // Construct the call for the theme function to run on this.
  $output['rdbanners_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $row,
    '#empty' => t('No Banners images found.'),
  );

  // Adds the pager at bottom of the table.
  $output['rdbanners_pager'] = array('#theme' => 'pager');

  // Let drupal handle print and echo.
  return $output;
}

/**
 * Function to delete images from rdbanners.
 */
function rdbanners_admin_delete_banner_image_form($form, &$form_state) {
  $form = array();
  $form['warning'] = array(
    '#markup' => check_plain(t('Are you sure, you want to delete banner image?')),
  );
  $form['rdbi_id'] = array(
    '#type' => 'hidden',
    '#default_value' => arg(4),
  );
  $form['action']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('delete'),
  );
  $form['action']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('cancel'),
  );
  return $form;
}

/**
 * Submit function to delete images from rdbanners.
 */
function rdbanners_admin_delete_banner_image_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];

  switch ($op) {
    case 'delete':
      $query = db_select('rdbanners_images', 'rdbi');
      $query->fields('rdbi', array('fid'))
          ->condition('rdbi_id', $form_state['values']['rdbi_id']);

      $result = $query->execute();
      $row = $result->fetchAssoc();

      $file = file_load($row['fid']);
      file_delete($file);

      $qry = db_delete('rdbanners_images');
      $qry->condition('rdbi_id', $form_state['values']['rdbi_id'], '=')
          ->execute();

      drupal_set_message(filter_xss(t('Banner images have been deleted successfully.')));
      $form_state['redirect'] = 'admin/structure/rdbanners';
      break;

    case 'cancel':
      $form_state['redirect'] = 'admin/structure/rdbanners';
      break;
  }
}
